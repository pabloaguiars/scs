class Ciclador {
public:
  unsigned long milisegundos;
  unsigned long tiempo;
  void comenzar();
  void terminar();
  Ciclador(unsigned long milisegundos);
};

class Evento {
private:
  int inicializado = 0;
  unsigned long activacion = 0;

public:
  unsigned long desfase = 0;
  unsigned long milisegundos = 0;
  virtual void disparar() = 0;
  void esperar(Ciclador ciclador);
};

class Sensor: public Evento {
public:
    void disparar();
};

class Led: public Evento {
public:
    void disparar();  
};

class Semaforo: public Evento {
public:
    Sensor sensor;
    Led verde;
    Led amarillo;
    Led rojo;
    void disparar();
    Semaforo(Sensor sensor, Led verde, Led amarillo, Led rojo);
};

class Interseccion {
public:
    Semaforo s1;
    Semaforo s2;
    Semaforo s3;
    Semaforo s4;
    void esperar(Ciclador ciclador);
};

class Transmisor: public Evento {
public:
    void disparar();  
};

class Receptor: public Evento {
public:
    void disparar();  
};