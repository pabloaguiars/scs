/*
  Interseccion.cpp - Librería con todas las clases necesarias para programar la intersección en arduino uno.
  Created by Pablo Aguiar, Santiago Aguila y Oscar Harper, Noviembre 16, 2018.
  Released into the public domain.
*/

#include "Interseccion.h"
#include "ArduinoJson.h"

Ciclador::Ciclador(unsigned long milisegundos) {
  _milisegundos = milisegundos;
};

void Ciclador::comenzar() {
  _tiempo = millis();
}

void Ciclador::terminar() {
  delay(_milisegundos);
}

void Evento::esperar(Ciclador ciclador) {
  if (!_inicializado) {
    _inicializado = 1;
    _activacion = ciclador._tiempo + _desfase + _milisegundos;
  }
  if (ciclador._tiempo >= _activacion) {
    _activacion = _activacion + _milisegundos;
    disparar();
  }
}

void EventoLed::disparar() {
  //apagamos leds y los volvemos a encender, cada cierta cantidad de tiempo
  apagarLeds();
  encenderLeds();
}

void EventoLed::apagarLeds() {
  //apagar leds del semáforo
  digitalWrite(_pin_uno, LOW);
  digitalWrite(_pin_dos, LOW);
}

void EventoLed::encenderLeds() {
  //reflejar estados del semáforo en el circuito fisico
  if((*_semaforo)._estado == 0){
      //cambiamos a rojo
      digitalWrite(_pin_uno, HIGH);
      digitalWrite(_pin_dos, LOW);
  } else if((*_semaforo)._estado == 1){
      //cambiamos a amarillo
      digitalWrite(_pin_uno, LOW);
      digitalWrite(_pin_dos, HIGH);
  } else if((*_semaforo)._estado == 2){
      //cambiamos a verde
      digitalWrite(_pin_uno, HIGH);
      digitalWrite(_pin_dos, HIGH);
  } else {
      //error
  }
}

void EventoSemaforo::disparar(){
  //cambiamos estados del semáforo
  cambiar();
}

void EventoSemaforo::cambiar(){
    if((*_semaforo)._permiso == true){
        //Si el semáforo tiene permisos para cambiar
        if((*_semaforo)._estado == 0){
            if((*_semaforo)._estado_anterior == 1){
                //Si el semaforo está en rojo, pasamos a verde
                (*_semaforo)._estado_anterior = (*_semaforo)._estado;
                (*_semaforo)._estado = 2;
                _milisegundos = (*_semaforo)._semiciclo;
            }
        } else if((*_semaforo)._estado == 1){
            if((*_semaforo)._estado_anterior == 2){
                //Sino, está en amarillo, pasamos a rojo.
                (*_semaforo)._estado_anterior = (*_semaforo)._estado;
                (*_semaforo)._estado = 0;
                _milisegundos = (*_semaforo)._ciclo - (*_semaforo)._semiciclo;
            }
        } else if((*_semaforo)._estado == 2){
            if((*_semaforo)._estado_anterior == 0){
                //Sino, está en verde, pasamos a amarillo
                (*_semaforo)._estado_anterior = (*_semaforo)._estado;
                (*_semaforo)._estado = 0;
                _milisegundos = 1000;
            }
        } else {
            //error
        }
    }
}

void EventoSensor::disparar() {
  //censamos
  censar();
}

void EventoSensor::censar() {
  _valor = analogRead(_pin_analogico);
  if(_valor > 900){
    //sin obstaculo
    _contador_tiempo_no_obstaculo = _contador_tiempo_no_obstaculo + 1;
    _acumulador_tiempo_no_obstaculo = _acumulador_tiempo_no_obstaculo + _valor;
    _promedio_tiempo_no_obstaculo = _acumulador_tiempo_no_obstaculo / _contador_tiempo_no_obstaculo;
  } else {
    //obstaculo detectado
    _contador_tiempo_obstaculo = _contador_tiempo_obstaculo + 1;
    _acumulador_tiempo_obstaculo = _acumulador_tiempo_obstaculo + _valor;
    _promedio_tiempo_obstaculo = _acumulador_tiempo_obstaculo / _contador_tiempo_obstaculo;
  }  
}

void EventoSensor::reiniciar() {
  //reiniciamos valores del censo
  _promedio_tiempo_obstaculo = 0;
  _contador_tiempo_obstaculo = 0;
  _acumulador_tiempo_obstaculo = 0;
  _promedio_tiempo_no_obstaculo = 0;
  _contador_tiempo_no_obstaculo = 0;
  _acumulador_tiempo_no_obstaculo = 0;
}

EventoTransmisor::EventoTransmisor(SoftwareSerial *serial){
  _serial = serial;
  
}

void EventoTransmisor::disparar() {
  //std::cout << "Enviando " << (mensaje) << "\n";
  serializar();
}

void EventoTransmisor::serializar(){
  //serializador
  const int capacity = JSON_OBJECT_SIZE(9);
  StaticJsonBuffer<capacity> jb;
  JsonObject& obj = jb.createObject();
  //Datos a enviar
  obj["id_interseccion"] = (*_interseccion)._id_interseccion;
  obj["modo"] = (*_interseccion)._modo;
  obj["ciclo"] = (*_interseccion)._ciclo;
  obj["semiciclo"] = (*_interseccion)._semiciclo;
  /*
  obj["tiempo_verde_s1"] = ;
  obj["tiempo_rojo_s1"] = ;
  obj["tiempo_verde_s2"] = ;
  obj["tiempo_rojo_s2"] = ;
  obj["tiempo_verde_s3"] = ;
  obj["tiempo_rojo_s3"] = ;
  obj["tiempo_verde_s4"] = ;
  obj["tiempo_rojo_s4"] = ;
  */
  
  //Compute the length of the minified JSON document
  int len1 = obj.measureLength();
  
  // Declare a buffer to hold the result
  char output[len1];

  // Produce a minified JSON document
  obj.printTo(output, sizeof(output));
  
  //Wrap the JsonArray in a JsonVariant
  JsonVariant v = obj;
  // Cast the JsonVariant to a string
  String outputString = v.as<String>();

  //enviamos datos por el puerto serial
  _serial.write(outputString);
}

EventoReceptor::EventoReceptor(SoftwareSerial *serial){
  _serial = serial;
}

void EventoReceptor::disparar() {
  //std::cout << "Enviando " << (mensaje) << "\n";
  //revisamos serial
  revisarSerial();
  if(_mensaje != ""){
    //Si hubo msj, deserializarlo y aplicar cambios
    deserializar();
  }
}

void EventoReceptor::revisarSerial(){
  //Mientras haya contenido en el puerto serial
  delay(10);
  while(_serial.available() > 0 ){
      char dato = _serial.read();
      delay(10);
      //vamos creando el msj
      _mensaje = _mensaje + dato;
  }
}

void EventoReceptor::deserializar(){
  const int capacity = JSON_OBJECT_SIZE(3); //resivimos maximo 3 objetos
  StaticJsonBuffer<capacity> jb; //objeto tipo jason buffer
  JsonObject& obj = jb.parseObject(_mensaje); //objeto json
  if (obj.success()) {
    // parseObject() succeeded
    if(obj["tiempos"] == obj["accion"]) {
      //cambiamos los tiempo
      if(obj["ciclo"] > 0){
        //tiempo ciclo
        _ciclo = obj["tiempo_ciclo"];
      } else {
        
      }

      if(obj["semiciclo"]  > 0){
          //tiempo semiciclo
          _semiciclo = obj["tiempo_semiciclo"];
      } else {
        
      }

    } else if(obj["modos"] == obj["accion"]){
      //cambiamos modo
      if(obj["modo"] == "auto" || obj["modo"] == "ciclo"){
        _modo = obj["modo"];
      } else {

      }

    } else if(obj["cambio"] == obj["accion"]){
      //cambio manual


    } else {

    }
    /*if(obj["permiso"] != -1){
        _permiso = obj["permiso"];
    }*/
  } else {
    // parseObject() failed
    
  }
  //Limpiamos el msj
  _mensaje = "";
}

Interseccion::Interseccion(int id_interseccion, int ciclo, int semiciclo, String modo){
  _id_interseccion = id_interseccion;
  _ciclo = ciclo;
  _semiciclo = semiciclo;
  _modo = modo;
  _permiso = false;

  s1 = new Semaforo(2,3,1,1000,500);
  s2 = new Semaforo(4,5,2,1000,500);
  s3 = new Semaforo(6,7,3,1000,500);
  s4 = new Semaforo(8,9,4,1000,500);
}

void Interseccion::esperar(Ciclador ciclador) {
  
  s1.esperar(ciclador);
  s2.esperar(ciclador);
  s3.esperar(ciclador);
  s4.esperar(ciclador);
}

Semaforo::Semaforo(int pin_uno, int pin_dos, int pin_sensor, int ciclo, int semiciclo) {

  _estado = 0;
  _estado_anterior = 1;
  _permiso = false;
  _ciclo = ciclo;
  _semiciclo = semiciclo; //tiempo en verde

  /*_delay_rojo_BACKUP = _delay_rojo;
  int _delay_amarillo_BACKUP = _delay_amarillo;
  int _delay_verde_BACKUP = _delay_verde;*/

  leds._semaforo = this;
  leds._pin_uno = pin_uno;
  leds._pin_dos = pin_dos;
  leds._desfase = 0;
  leds._milisegundos = 20;

  sensor._pin_analogico = pin_sensor;
  sensor._desfase = 0;
  sensor._milisegundos = 10;
  sensor._promedio_tiempo_obstaculo = 0;
  sensor._contador_tiempo_obstaculo = 0;
  sensor._acumulador_tiempo_obstaculo = 0;
  sensor._promedio_tiempo_no_obstaculo = 0;
  sensor._contador_tiempo_no_obstaculo = 0;
  sensor._acumulador_tiempo_no_obstaculo = 0;

  semaforo._semaforo = this;
  semaforo._desfase = 0;
  semaforo._milisegundos = _semiciclo;
}

void Semaforo::esperar(Ciclador ciclador) {
  leds.esperar(ciclador);
  sensor.esperar(ciclador);
  semaforo.esperar(ciclador);
}