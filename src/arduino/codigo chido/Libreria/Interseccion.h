/*
  Interseccion.h - Librería con todas las clases necesarias para programar la intersección en arduino uno.
  Created by Pablo Aguiar, Santiago Aguila y Oscar Harper, Noviembre 16, 2018.
  Released into the public domain.
*/

#ifndef Interseccion_h
#define Interseccion_h

#include "Arduino.h"
#include "SoftwareSerial.h"

//Clase CICLADOR
class Ciclador {
public:
  unsigned long _milisegundos;
  unsigned long _tiempo;
  void comenzar();
  void terminar();
  Ciclador(unsigned long milisegundos);
};

//Clase EVENTO
class Evento {
private:
  int _inicializado = 0;
  unsigned long _activacion = 0;

public:
  unsigned long _desfase = 0;
  unsigned long _milisegundos = 0;
  virtual void disparar() = 0;
  void esperar(Ciclador ciclador);
};

class Interseccion;
//Clase para enviar msj
class EventoTransmisor: public Evento {
public:
  String _mensaje;
  String _mensaje_serializado;
  EventoTransmisor(SoftwareSerial *serial);
  void disparar();
  void serializar();

  SoftwareSerial *_serial;

  Interseccion *_interseccion;
};

//Clase para recibir msj
class EventoReceptor: public Evento {
public:
  String _mensaje;
  String _mensaje_deserializado;
  EventoReceptor(SoftwareSerial *serial);
  void disparar();
  void revisarSerial();
  void deserializar();

  SoftwareSerial *_serial;


};

class Semaforo;
//Clase Evento de los leds
class EventoLed: public Evento {
public:
  int _pin_uno;
  int _pin_dos;
  Semaforo *_semaforo;
  void disparar();
  void apagarLeds();
  void encenderLeds();
};

//Clase Evento del semáforo
class EventoSemaforo: public Evento {
public:
  Semaforo *_semaforo;
  void disparar();
  void cambiar();
};

//Clase Evento del sensor
class EventoSensor: public Evento {
public:
  int _pin_analogico;
  int _promedio_tiempo_obstaculo;
  int _contador_tiempo_obstaculo;
  int _acumulador_tiempo_obstaculo;
  int _promedio_tiempo_no_obstaculo;
  int _contador_tiempo_no_obstaculo;
  int _acumulador_tiempo_no_obstaculo;
  int _valor;
  void disparar();
  void censar();
  void reiniciar();
};


//Clase SEMAFORO
class Semaforo {
public:
  EventoLed leds;
  EventoSensor sensor;
  EventoSemaforo semaforo;
  void esperar(Ciclador ciclador);
  Semaforo(int pin_uno, int pin_dos, int pin_sensor, int ciclo, int semiciclo);

  int _estado; //0:rojo, 1:amarillo, 2:verde
  int _estado_anterior; 
  bool _permiso; //_permiso para cambiar de estado 0:no 1:sí
  int _ciclo;
  int _semiciclo; //tiempo en verde
  String _modo;

  /*int _delay_rojo_BACKUP;
  int _delay_amarillo_BACKUP;
  int _delay_verde_BACKUP;*/
};

class Interseccion{
public:
  Interseccion(int id_interseccion, int ciclo, int semiciclo, String modo);
  void esperar(Ciclador Ciclador);

  int _id_interseccion;
  bool _permiso; //_permiso para cambiar de estado 0:no 1:sí
  int _ciclo;
  int _semiciclo; //tiempo en verde
  String _modo;
  
  Semaforo *s1;
  Semaforo *s2;
  Semaforo *s3;
  Semaforo *s4;

};

#endif
