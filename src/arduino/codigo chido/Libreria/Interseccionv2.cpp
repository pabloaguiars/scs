#include <Arduino.h>
#include <Interseccionv2.h>

Ciclador::Ciclador(unsigned long milisegundos) {
  this->milisegundos = milisegundos;
};

void Ciclador::comenzar() {
  tiempo = millis();
}

void Ciclador::terminar() {
  delay(milisegundos);
}

void Evento::esperar(Ciclador ciclador) {
  if (!inicializado) {
    inicializado = 1;
    activacion = ciclador.tiempo + desfase + milisegundos;
  }
  if (ciclador.tiempo >= activacion) {
    activacion = activacion + milisegundos;
    disparar();
  }
}

Semaforo::Semaforo(Sensor sensor, Led verde, Led amarillo, Led rojo) {
    this->sensor = sensor;
    this->verde = verde;
    this->amarillo = amarillo;
    this->rojo = rojo;
}

Semaforo::disparar() {
    
}

class Semaforo: public Evento {
public:
    Sensor sensor;
    Led verde;
    Led amarillo;
    Led rojo;
    void disparar();
};