 /*
  Firmware_arduino_uno.ino - Código necesario para intersección en arduino uno.
  Created by Pablo Aguiar, Santiago Aguila y Oscar Harper, Noviembre 16, 2018.
  Released into the public domain.
*/

#include <Arduino.h>
#include <ArduinoJson.h>
#include <Interseccionv2.h>
#include <SoftwareSerial.h>

//Instacia de las clases
SoftwareSerial serial(0,1); //RX / TX
EventoTransmisor *transmisor;
EventoReceptor *receptor;


Interseccion interseccion(1,5000,3000,"auto");

Ciclador ciclador(10);

void setup() {
  // put your setup code here, to run once:
  
  serial.begin(9600);
  transmisor = new EventoTransmisor((&serial));
  receptor = new EventoReceptor((&serial));

  transmisor->_desfase = 0;
  transmisor->_milisegundos = 100;

  receptor->_desfase = 100;
  receptor->_milisegundos = 100;
}

void loop() {
  // put your main code here, to run repeatedly:
  while (true) {
    ciclador.comenzar();
    interseccion.esperar(ciclador);
    s1.esperar(ciclador);
    s2.esperar(ciclador);
    s3.esperar(ciclador);
    s4.esperar(ciclador);
    ciclador.terminar();
  }
}
