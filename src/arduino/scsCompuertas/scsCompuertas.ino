#include "semaforo.h"
#include <SoftwareSerial.h>
#include "ArduinoJson.h"
#include <stdio.h>
#include <string.h>

SoftwareSerial serial(0, 1); // RX, TX

Semaforo s1(2, 3, 0, 12000, 6000, 2);
Semaforo s2(4, 5, 1, 12000, 6000, 1);
Semaforo s3(6, 7, 2, 12000, 6000, 2);
Semaforo s4(8, 9, 3, 12000, 6000, 1);

int id_interseccion = 9;
char modo = "ciclico";

String msj_serializador = "";
String msj_deserializador = "";

void setup()
{
  serial.begin(9600); //9600 bluetooth esclavo | 38400 bluetooth maestro esclavo
}
void loop()
{

  unsigned long tiempo_actual = millis();
  s1.Trabajar(tiempo_actual);
  s2.Trabajar(tiempo_actual);
  s3.Trabajar(tiempo_actual);
  s4.Trabajar(tiempo_actual);
  if (s1.ciclocompleto && s2.ciclocompleto && s3.ciclocompleto && s4.ciclocompleto)
  {
    if (serial.available() > 0)
    {
      //serial.println("---");
      //si hay, entramos en un ciclo hasta leer todo el msj
      while (serial.available() > 0)
      {
        char Dato = serial.read(); //leeemos datos
        delay(10);
        //serial.println(Dato);
        msj_deserializador = msj_deserializador + Dato; //guardamos dato
      }

      deserializar();
      //serial.println("---");
      msj_deserializador = "";
    }

    s1.ciclocompleto = false;
    s2.ciclocompleto = false;
    s3.ciclocompleto = false;
    s4.ciclocompleto = false;
    serializar();
  }
}

void serializar()
{
  //serializador
  //const int capacity = JSON_ARRAY_SIZE(1) + 2*JSON_OBJECT_SIZE(3);
  const int capacity = JSON_OBJECT_SIZE(9); //cuánto objetos tendrá el json
  StaticJsonBuffer<capacity> jb;

  // Create a JsonObject
  //JsonArray& arr = jb.createArray();

  JsonObject &obj = jb.createObject();
  //almacenamos información

  //Datos a enviar
  obj["id_interseccion"] = id_interseccion;
  obj["modo"] = modo;
  obj["ciclo"] = s1._ciclo;
  obj["semiciclo"] = s1._semiciclo;

  obj["tiempo_verde_s1"] = s1._Sensor->promedio_verde;
  obj["tiempo_rojo_s1"] = s1._Sensor->promedio_rojo;
  obj["tiempo_verde_s2"] = s2._Sensor->promedio_verde;
  obj["tiempo_rojo_s2"] = s2._Sensor->promedio_rojo;
  obj["tiempo_verde_s3"] = s3._Sensor->promedio_verde;
  obj["tiempo_rojo_s3"] = s3._Sensor->promedio_rojo;
  obj["tiempo_verde_s4"] = s4._Sensor->promedio_verde;
  obj["tiempo_rojo_s4"] = s4._Sensor->promedio_rojo;

  //Compute the length of the minified JSON document
  int len1 = obj.measureLength();

  // Declare a buffer to hold the result
  char output[len1];

  // Produce a minified JSON document
  obj.printTo(output, sizeof(output));

  //Wrap the JsonArray in a JsonVariant
  JsonVariant v = obj;
  // Cast the JsonVariant to a string
  String outputString = v.as<String>();

  // Print a minified version to the serial port
  //enviar información
  serial.println(outputString); //println o write? :/
  delay(10);
}

void deserializar()
{
  //deserializador
  serial.println("msj_deserializador: " + msj_deserializador);
  char *input = new char[msj_deserializador.length() + 1];
  strcpy(input, msj_deserializador.c_str());

  const int capacity = JSON_OBJECT_SIZE(3);
  StaticJsonBuffer<capacity> jb;

  JsonObject &obj = jb.parseObject(input);
  // put your main code here, to run repeatedly:
  if (obj.success())
  {
    // parseObject() succeeded
    serial.println("hecho");

    if (obj["ciclo"] > 0)
    {
      //tiempo ciclo
      s1._ciclo = obj["ciclo"];
      s2._ciclo = obj["ciclo"];
      s3._ciclo = obj["ciclo"];
      s4._ciclo = obj["ciclo"];
      serial.println(s1._ciclo);
    }

    if (obj["semiciclo"] < s1._ciclo)
    {
      s1._semiciclo = obj["semiciclo"];
      s2._semiciclo = obj["semiciclo"];
      s3._semiciclo = obj["semiciclo"];
      s4._semiciclo = obj["semiciclo"];
    }
  }
  if (obj["modo"] == "auto" || obj["modo"] == "ciclo")
  {
    modo = obj["modo"];
  }

  else
  {
    // parseObject() failed Chapter 3 Deserialize with ArduinoJson 64
    serial.println("error");
  }
}
