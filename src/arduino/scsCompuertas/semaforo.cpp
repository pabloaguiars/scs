#include <arduino.h>
#include "semaforo.h"

Semaforo::Semaforo(int pin_10, int pin_01, int pin_sensor, int ciclo, int semiciclo, int estado) 
{
  //constructor semáforo
  pinMode(pin_10, OUTPUT);
  pinMode(pin_01, OUTPUT);

  _pin_10 = pin_10;
  _pin_01 = pin_01;
  _estado = estado;
  _estado_inicial = estado;
  _ciclo = ciclo;
  _semiciclo = semiciclo;
  _tiempo_amarillo = 2000;

  ciclocompleto = false;
  _contador = 0;
  _tiempo_previo = 0;

  _Sensor = new Sensor(pin_sensor);
  SiguienteEstado();
}

void Semaforo::SiguienteEstado()
{
  // verde amarillo
  if (_estado == 0)
  {
    _estado = 1;
    digitalWrite(_pin_10, HIGH);
    digitalWrite(_pin_01, LOW);

  }
  // amarillo a rojo
  else if (_estado == 1)
  {
    _estado = 2;
            digitalWrite(_pin_10, LOW);
    digitalWrite(_pin_01, HIGH);


  }
  // rojo a verde
  else if (_estado == 2)
  {
    _estado = 0;
            digitalWrite(_pin_10, HIGH);
    digitalWrite(_pin_01, HIGH);

  }
}

int Semaforo::GetEstado()
{
  return _estado;
}

void Semaforo::Trabajar(unsigned long tiempo_actual)
{


  if (GetEstado() == 0 && (tiempo_actual - _tiempo_previo >= _semiciclo - _tiempo_amarillo))
  {
    SiguienteEstado();
    _tiempo_previo = tiempo_actual;
  }

  if (GetEstado() == 2 && (tiempo_actual - _tiempo_previo >= _semiciclo))
  {
    _Sensor->Promedio(_contador, true);
    SiguienteEstado();
    if(_estado_inicial == 1) ciclocompleto = true;
    _contador = 0;
    _tiempo_previo = tiempo_actual;
  }

  if (GetEstado() == 1 && (tiempo_actual - _tiempo_previo >= _tiempo_amarillo))
  {
    _Sensor->Promedio(_contador, false);
    SiguienteEstado();
    if(_estado_inicial == 2) ciclocompleto = true;
    _contador = 0;
    _tiempo_previo = tiempo_actual;
  }

  _contador += 1;
  _Sensor->Valor();
}


// void Semaforo::SetEstado(int estado)
// {
//   _estado = estado;
//   Trabajar();
// }
