#include <arduino.h>
#include "sensor.h"

Sensor::Sensor()
{
}
Sensor::Sensor(int pin)
{
  _pin = pin;
  _suma = 0;
  promedio_verde = 0;
  promedio_rojo = 0;
}

void Sensor::Valor()
{
  if (analogRead(_pin) < 110)
  {
      _suma += 1;
  }
}

void Sensor::Promedio(int count, bool verde)
{
  if (verde == true)
  {
    promedio_verde = (double)_suma / (double)count;
    _suma = 0;
  }
  else
  {
    promedio_rojo = (double)_suma / (double)count;
    _suma = 0;
  }
}