#include "sensor.h"

class Semaforo
{
public:
  Semaforo(int pin_10, int pin_01, int estado, int pin_sensor, int ciclo, int semiciclo);
  void Trabajar(unsigned long tiempo_actual);
  bool ciclocompleto;
  Sensor *_Sensor;

  int _pin_10;
  int _pin_01;
  int _estado;
  int _estado_inicial;
  int _ciclo;
  int _semiciclo;
  int _tiempo_amarillo;
  int _contador;
  unsigned long _tiempo_previo;
  int GetEstado();
  void SiguienteEstado();

private:
  
};
