#include <arduino.h>
#include "semaforo.h"

Semaforo::Semaforo(int pin_verde, int pin_amarillo, int pin_rojo, int pin_sensor, int ciclo, int semiciclo, int estado) 
{
  //constructor semáforo
  pinMode(pin_verde, OUTPUT);
  pinMode(pin_amarillo, OUTPUT);
  pinMode(pin_rojo, OUTPUT);

  _pin_verde = pin_verde;
  _pin_amarillo = pin_amarillo;
  _pin_rojo = pin_rojo;
  _estado = estado;
  _estado_inicial = estado;
  _ciclo = ciclo;
  _semiciclo = semiciclo;
  _tiempo_amarillo = 2000;

  ciclocompleto = false;
  _contador = 0;
  _tiempo_previo = 0;

  _Sensor = new Sensor(pin_sensor);
  SiguienteEstado();
}

void Semaforo::SiguienteEstado()
{
  // verde amarillo
  if (_estado == 0)
  {
    _estado = 1;
    digitalWrite(_pin_verde, LOW);
    digitalWrite(_pin_amarillo, HIGH);
    digitalWrite(_pin_rojo, LOW);
  }
  // amarillo a rojo
  else if (_estado == 1)
  {
    _estado = 2;
    digitalWrite(_pin_verde, LOW);
    digitalWrite(_pin_amarillo, LOW);
    digitalWrite(_pin_rojo, HIGH);
  }
  // rojo a verde
  else if (_estado == 2)
  {
    _estado = 0;
    digitalWrite(_pin_verde, HIGH);
    digitalWrite(_pin_amarillo, LOW);
    digitalWrite(_pin_rojo, LOW);
  }
}

int Semaforo::GetEstado()
{
  return _estado;
}

void Semaforo::Trabajar(unsigned long tiempo_actual)
{


  if (GetEstado() == 0 && (tiempo_actual - _tiempo_previo >= _semiciclo - _tiempo_amarillo))
  {
    SiguienteEstado();
    _tiempo_previo = tiempo_actual;
  }

  if (GetEstado() == 2 && (tiempo_actual - _tiempo_previo >= _semiciclo))
  {
    _Sensor->Promedio(_contador, true);
    SiguienteEstado();
    if(_estado_inicial == 1) ciclocompleto = true;
    _contador = 0;
    _tiempo_previo = tiempo_actual;
  }

  if (GetEstado() == 1 && (tiempo_actual - _tiempo_previo >= _tiempo_amarillo))
  {
    _Sensor->Promedio(_contador, false);
    SiguienteEstado();
    if(_estado_inicial == 2) ciclocompleto = true;
    _contador = 0;
    _tiempo_previo = tiempo_actual;
  }

  _contador += 1;
  _Sensor->Valor();
}


// void Semaforo::SetEstado(int estado)
// {
//   _estado = estado;
//   Trabajar();
// }
