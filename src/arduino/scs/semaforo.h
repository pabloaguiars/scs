#include "sensor.h"

class Semaforo
{
public:
  Semaforo(int pin_verde, int pin_amarillo, int pin_rojo, int estado, int pin_sensor, int ciclo, int semiciclo);
  void Trabajar(unsigned long tiempo_actual);
  bool ciclocompleto;
  Sensor *_Sensor;

  int _pin_verde;
  int _pin_amarillo;
  int _pin_rojo;
  int _estado;
  int _estado_inicial;
  int _ciclo;
  int _semiciclo;
  int _tiempo_amarillo;
  int _contador;
  unsigned long _tiempo_previo;
  int GetEstado();
  void SiguienteEstado();

private:
  
};
