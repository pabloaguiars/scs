#include <SoftwareSerial.h>

SoftwareSerial serial(0, 1); // RX, TX
int led = 13;

void setup(){
   serial.begin(38400);
   pinMode(led,OUTPUT);
}

void loop(){
  delay(10);
  while(serial.available() > 0 ){
      char Dato = serial.read();
      delay(10);
      serial.write(Dato);
      if(Dato == 'A')   digitalWrite(led,HIGH);
      else if(Dato == 'B') digitalWrite(led,LOW);
  }
}
