#include <ArduinoJson.h>


void setup() {  
  pinMode(8, OUTPUT);
  // put your setup code here, to run once:
  
}

void loop() {
  char input[] = "{\"asd1\":\"1\",\"asd2\":\"2\",\"asd3\":\"3\"}";  
  const int capacity = JSON_OBJECT_SIZE(3);
  StaticJsonBuffer<capacity> jb;
  
  JsonObject& obj = jb.parseObject(input);
  // put your main code here, to run repeatedly:
  if (obj.success()) {
    // parseObject() succeeded
    if(obj.get<double>("asd1") == 1){
        digitalWrite(8, HIGH);
        delay(1000);
        digitalWrite(8, LOW);
        delay(1000);
      } 
  } else {
    // parseObject() failed Chapter 3 Deserialize with ArduinoJson 64
    
  }
}
