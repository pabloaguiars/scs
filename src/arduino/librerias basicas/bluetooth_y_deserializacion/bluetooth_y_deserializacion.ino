#include <SoftwareSerial.h>
#include <ArduinoJson.h>
#include <stdio.h>
#include <string.h>

SoftwareSerial serial(0, 1); // RX, TX
int led = 13;
String msj = "";

void setup(){
   serial.begin(38400);
   pinMode(led,OUTPUT);
}

void loop(){
  delay(10);
  if(serial.available() > 0){
    //serial.println("---");
    while(serial.available() > 0 ){
      char Dato = serial.read();
      delay(10);
      //serial.println(Dato);
      msj = msj + Dato;
    }
    
    serializar();
    //serial.println("---");
    msj="";
  }
}

void serializar(){
  serial.println("msj: " + msj);
  char * input = new char [msj.length()+1];
  strcpy (input, msj.c_str());
  
  const int capacity = JSON_OBJECT_SIZE(3);
  StaticJsonBuffer<capacity> jb;
  
  JsonObject& obj = jb.parseObject(input);
  // put your main code here, to run repeatedly:
  if (obj.success()) {
    // parseObject() succeeded
    serial.println("hecho");
    
    if(obj.get<int>("asd1") == 1){
      serial.println("asd1");
        //digitalWrite(led, HIGH);
        //delay(1000);
        //digitalWrite(led, LOW);
        //delay(1000);
    } 
    if(obj.get<int>("asd2") == 2){
      serial.println("asd2");
    } 
    if(obj.get<int>("asd3") == 3){
      serial.println("asd2");
    }
  } else {
    // parseObject() failed Chapter 3 Deserialize with ArduinoJson 64
    serial.println("error");
  }
}
