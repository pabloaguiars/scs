#include "ciclador.cpp"

class EventoLed: public Evento {
public:
  int led;
  void disparar();
};

void EventoLed::disparar() {
  std::cout << "Prender LED " << led << "\n";
}

class Semaforo {
public:
  EventoLed verde, amarillo, rojo;
  void esperar(Ciclador ciclador);
  Semaforo();
};

Semaforo::Semaforo() {
  verde.led = 1;
  verde.desfase = 0;
  verde.milisegundos = 300;

  amarillo.led = 2;
  amarillo.desfase = 100;
  amarillo.milisegundos = 300;

  rojo.led = 3;
  rojo.desfase = 200;
  rojo.milisegundos = 300;
}

void Semaforo::esperar(Ciclador ciclador) {
  verde.esperar(ciclador);
  amarillo.esperar(ciclador);
  rojo.esperar(ciclador);
}

int main() {
  Semaforo semaforo;
  Ciclador ciclador(100);

  while (true) {
    ciclador.comenzar();
    semaforo.esperar(ciclador);
    ciclador.terminar();
  }
  return 0;
}
