class Ciclador {
public:
  unsigned long milisegundos;
  unsigned long tiempo;
  void comenzar();
  void terminar();
  Ciclador(unsigned long milisegundos);
};

Ciclador::Ciclador(unsigned long milisegundos) {
  this->milisegundos = milisegundos;
};

void Ciclador::comenzar() {
  tiempo = millis();
}

void Ciclador::terminar() {
  delay(milisegundos);
}

class Evento {
private:
  int inicializado = 0;
  unsigned long activacion = 0;

public:
  unsigned long desfase = 0;
  unsigned long milisegundos = 0;
  virtual void disparar() = 0;
  void esperar(Ciclador ciclador);
};

void Evento::esperar(Ciclador ciclador) {
  if (!inicializado) {
    inicializado = 1;
    activacion = ciclador.tiempo + desfase + milisegundos;
  }
  if (ciclador.tiempo >= activacion) {
    activacion = activacion + milisegundos;
    disparar();
  }
}
