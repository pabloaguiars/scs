# Ciclador
Es una biblioteca de código de C++ para poder ciclar eventos en un microcontrolador. Consta de un ciclador y eventos que son ejecutados dentro de un ciclo.

Ejecutar el ejemplo da el siguiente output:
```
Prender LED 1
Prender LED 2
Prender LED 3
Prender LED 1
Prender LED 2
Prender LED 3
Prender LED 1
Prender LED 2
Prender LED 3
Prender LED 1
Prender LED 2
Prender LED 3
Prender LED 1
Prender LED 2
Prender LED 3
Prender LED 1
Prender LED 2
```
Funciona de la forma descrita en el diagrama:

![diagrama](diagrama.jpg)
