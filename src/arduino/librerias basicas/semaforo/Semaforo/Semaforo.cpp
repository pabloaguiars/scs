/*
  Semaforo.cpp - Librería de semáforos.
  Created by Pablo Aguiar, November 16, 2018.
  Released into the public domain.
*/

#include "Arduino.h"
#include "Semaforo.h"

Semaforo::Semaforo(int pin_01, int pin_10, int delay_rojo, int delay_amarillo, int delay_verde){ 
    //constructor semáforo
    pinMode(pin_01, OUTPUT);
    pinMode(pin_10, OUTPUT);

    _estado = 0;
    _estado_anterior = 1;
    _permiso = false;
    _pin_01 = pin_01;
    _pin_10 = pin_10;
    _delay_rojo = delay_rojo;
    _delay_amarillo = delay_amarillo;
    _delay_verde = delay_verde;
}

void Semaforo::Permiso(bool permiso){
    if(permiso == true){
        //_permisos para cambiar
        _permiso = true;
    } else {
        //Le quitamos _permisos para cambiar
        _permiso = false;  
    }
}

void Semaforo::Trabajar(){
    if(_permiso == true){
        //Si tiene permisos para trabajar, comienza
        ReflejarEstado();
        Cambiar();
    }
}

void Semaforo::ReflejarEstado(){
    if(_permiso == true){
        //Si tiene _permiso para cambiar, refleja el _estado
        if(_estado == 0){
            //cambiamos a rojo
            digitalWrite(_pin_01, HIGH);
            digitalWrite(_pin_10, LOW);
            delay(_delay_rojo);
        } else if(_estado == 1){
            //cambiamos a amarillo
            digitalWrite(_pin_01, LOW);
            digitalWrite(_pin_10, HIGH);
            delay(_delay_amarillo);
        } else if(_estado == 2){
            //cambiamos a verde
            digitalWrite(_pin_01, HIGH);
            digitalWrite(_pin_10, HIGH);
            delay(_delay_verde);
        } else {
            //error
        }
    }
}

void Semaforo::Cambiar(){
    if(_permiso == true){
        //Si el semáforo tiene permisos para cambiar
        if(_estado == 0){
            if(_estado_anterior == 1){
                //Si el semaforo está en rojo, pasamos a verde
                _estado_anterior = _estado;
                _estado = 2;
            }
        } else if(_estado == 1){
            if(_estado_anterior == 2){
                //Sino, está en amarillo, pasamos a rojo.
                _estado_anterior = _estado;
                _estado = 0;
            }
        } else if(_estado == 2){
            if(_estado_anterior == 0){
                //Sino, está en verde, pasamos a amarillo
                _estado_anterior = _estado;
                _estado = 1;
            }
        } else {
            //error
        }
    }
}

int Semaforo::ObtenerEstado(){
    //devolvemos _estado
    return(_estado);
}

bool Semaforo::ObtenerPermiso(){
    //devolvemos _permiso
    return(_permiso);
}

int Semaforo::ObtenerPin(int luz){
    //devolvemos el pin solicitado 0:rojo, 1:amarillo y 2:verde
    int resultado;
    if(luz == 0){
        resultado = _pin_01;
    } else if(luz == 1){
        resultado = _pin_10;
    } else if(luz == 2){
   //     resultado = _pin_verde;
    } else {
        //error
        resultado = 3;
    }
    return(resultado);
}

int Semaforo::ObtenerTiempo(int luz){
    //devolvemos el tiempo solicitado 0:rojo, 1:amarillo y 2:verde
    int resultado;
    if(luz == 0){
        resultado = _delay_rojo;
    } else if(luz == 1){
        resultado = _delay_amarillo;
    } else if(luz == 2){
        resultado = _delay_verde;
    } else {
        //error
        resultado = 3;
    }
    return(resultado);
}