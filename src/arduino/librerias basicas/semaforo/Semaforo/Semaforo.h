/*
  Semaforo.h - Librería de semáforos.
  Created by Pablo Aguiar, Noviembre 16, 2018.
  Released into the public domain.
*/
#ifndef Semaforo_h
#define Semaforo_h

#include "Arduino.h"

class Semaforo
{
    public:
        Semaforo(int pin_01, int pin_10, int delay_rojo, int delay_amarillo, int delay_verde);
        void Permiso(bool permiso);
        void Cambiar();
        void ReflejarEstado();
        int ObtenerEstado();
        bool ObtenerPermiso();
        int ObtenerPin(int luz);
        int ObtenerTiempo(int luz);
        void LOCO();
        void Trabajar();
    private:
        int _estado; //0:rojo, 1:amarillo, 2:verde
        int _estado_anterior;
        bool _permiso; //_permiso para cambiar de estado 0:no 1:sí
        int _pin_01; //pin led rojo
        int _pin_10; //pin led amarillo
        int _delay_rojo; //delay en rojo
        int _delay_amarillo; //delay en amarillo
        int _delay_verde; //delay en verde
};

#endif