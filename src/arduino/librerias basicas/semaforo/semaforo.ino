#include <Arduino.h>
#include "Semaforo.h"

Semaforo semaforo(3,2,2000,1000,2000);

void setup() {
  // put your setup code here, to run once:
  semaforo.Permiso(true);
}

void loop() {
  // put your main code here, to run repeatedly:
  
  semaforo.Trabajar();
}
