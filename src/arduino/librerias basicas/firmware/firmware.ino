#include <Arduino.h>
#include <Semaforo.h>
#include <ArduinoJson.h>
#include <SoftwareSerial.h>

SoftwareSerial serial(0,1); //RX, TX PARA EL BLUETOOTH
String datos_recibidos = "";
String datos_enviar = "";

//variables de la intersección
int _id_interseccion = 0; //id de la intersección
bool _permiso = false; //permiso para trabajar 0:no 1:sí
int _tiempo_ciclo = 10000; //tiempo total del ciclo de la intersección en milisegundos
int _tiempo_semiciclo = 5000; //mitad del ciclo de la intersección en milisegundos
int _modo; //modo de operación de la intersección 0:ciclico, 1:manual, 2:automatico, 3:por horario
int _cuantos_semaforos = 4; //cuántos semaforos componen la intersección

//instancia de los semáforos
Semaforo s1(2,3,2000,1000,2000,1); //(pin digital semaforo 1, pin digital semaforo 2, delay rojo, delay amarillo, delay verde, pin analogo sensor)
Semaforo s2(4,5,2000,1000,2000,2);
Semaforo s3(6,7,2000,1000,2000,3);
Semaforo s4(8,9,2000,1000,2000,4);

void setup() {
  // put your setup code here, to run once:
  serial.begin(38400);
  s1.Permiso(true);
  s2.Permiso(true);
  s3.Permiso(true);
  s4.Permiso(true);

  s1.CambiarManualmente(0);
  s2.CambiarManualmente(2);
  s3.CambiarManualmente(0);
  s4.CambiarManualmente(2);
  
  Permiso(true);
}

void loop() {
  // put your main code here, to run repeatedly:
  while(serial.available() > 0){
    //Mientras haya datos, los almacenamos
    datos_recibidos = datos_recibidos + Serial.read();
    delay(10);
  }
  
  if(datos_recibidos != ""){
    //Deserializar
    Deserializar();
    Trabajar();
  } else {
    //No se recibieron datos
    Trabajar();
  }
}

void Serializar(){
  //serializador
  const int capacity = JSON_OBJECT_SIZE(9);
  StaticJsonBuffer<capacity> jb;
  JsonObject& obj = jb.createObject();
  //Datos a enviar
  obj["id_interseccion"] = _id_interseccion;
  obj["tiempo_verde_s1"] = s1.ObtenerPromedioSensor(1);
  obj["tiempo_rojo_s1"] = s1.ObtenerPromedioSensor(0);
  obj["tiempo_verde_s2"] = s2.ObtenerPromedioSensor(1);
  obj["tiempo_rojo_s2"] = s2.ObtenerPromedioSensor(0);
  obj["tiempo_verde_s3"] = s3.ObtenerPromedioSensor(1);
  obj["tiempo_rojo_s3"] = s3.ObtenerPromedioSensor(0);
  obj["tiempo_verde_s4"] = s4.ObtenerPromedioSensor(1);
  obj["tiempo_rojo_s4"] = s4.ObtenerPromedioSensor(0);
  
  //Compute the length of the minified JSON document
  int len1 = obj.measureLength();
  
  // Declare a buffer to hold the result
  char output[len1];

  // Produce a minified JSON document
  obj.printTo(output, sizeof(output));
  
  //Wrap the JsonArray in a JsonVariant
  JsonVariant v = obj;
  // Cast the JsonVariant to a string
  String outputString = v.as<String>();

  //enviamos datos por el puerto serial
  serial.write(outputString);
}

void Deserializar(){
  //deserializador
  
  //char* input[] = datos_recibidos;  
  const int capacity = JSON_OBJECT_SIZE(3); //REVISAR cuánto objetos voy a recibir?
  StaticJsonBuffer<capacity> jb;
  JsonObject& obj = jb.parseObject(datos_recibidos);
  if (obj.success()) {
    // parseObject() succeeded
    if(obj["tiempo_ciclo"] != -1){
        _tiempo_ciclo = obj["tiempo_ciclo"];
    }

    if(obj["tiempo_semiciclo"] != -1){
        _tiempo_semiciclo = obj["tiempo_semiciclo"];
    }

    if(obj["modo"] != -1){
        _modo = obj["modo"];
    }

    if(obj["permiso"] != -1){
        _permiso = obj["permiso"];
    }
  } else {
    // parseObject() failed
    
  }
}

void Trabajar(){
  //método para establecer el modo de operación 
  if(_modo == 0){
    //modo ciclico
    
  } else if(_modo == 1){
    //modo manual
  } else if(_modo == 2){
    //modo automatico
  } else if(_modo == 3){
    //modo por horario
  } else {
    
  }
}

void Cambiar(){
    //método para cambiar de luz semáforos
    if((s1.ObtenerEstado() == 2))
}

void ReflejarEstado(){
    //método para imprimir la luz de los semáforos
    s1.ReflejarEstado();
    s2.ReflejarEstado();
    s3.ReflejarEstado();
    s4.ReflejarEstado();
}

void Censar(){ //AGREGAR TIMER
  //método para pedir a los semáforos poner a sus sensores a censar
  s1.Censar();
  s2.Censar();
  s3.Censar();
  s4.Censar();
}

}

void Permiso(bool permiso){
  //método para cambiar el permiso de la intersección
  if(permiso == true){
    //_permisos para cambiar
    _permiso = true;
  } else {
    //Le quitamos _permisos para cambiar
    _permiso = false;  
  }
}
