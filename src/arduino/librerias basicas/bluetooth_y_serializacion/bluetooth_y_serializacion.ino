#include <SoftwareSerial.h>
#include <ArduinoJson.h>
#include <stdio.h>
#include <string.h>

SoftwareSerial serial(0, 1); // RX, TX
int led = 13;

void setup(){
   serial.begin(38400);
   pinMode(led,OUTPUT);
}

void loop(){
  delay(10);
  /*while(serial.available() > 0 ){
      char Dato = serial.read();
      delay(10);
      serial.write(Dato);
      if(Dato == 'A')   digitalWrite(led,HIGH);
      else if(Dato == 'B') digitalWrite(led,LOW);
  }*/

  // put your main code here, to run repeatedly:
  //const int capacity = JSON_ARRAY_SIZE(1) + 2*JSON_OBJECT_SIZE(3);
  const int capacity = JSON_OBJECT_SIZE(3);
  StaticJsonBuffer<capacity> jb;

  // Create a JsonObject
  //JsonArray& arr = jb.createArray();

  JsonObject& obj1 = jb.createObject();
  obj1["asd1"] = 1;
  obj1["asd2"] = 2;
  obj1["asd3"] = 3;
  
  //Compute the length of the minified JSON document
  int len1 = obj1.measureLength();
  
  // Declare a buffer to hold the result
  char output[len1];

  // Produce a minified JSON document
  obj1.printTo(output, sizeof(output));
  
  //Wrap the JsonArray in a JsonVariant
  JsonVariant v = obj1;
  // Cast the JsonVariant to a string
  String outputString = v.as<String>();

  // Print a minified version to the serial port
  serial.println(outputString);
  delay(3000);
}
