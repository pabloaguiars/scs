import { app, BrowserWindow, ipcMain } from "electron";
import axios from "axios";
import "core-js/fn/array/flat-map";

const FLASK_SERVER = process.env.FLASK_SERVER || "http://localhost:5000";

let win = null;
function createWindow() {
  win = new BrowserWindow({
    width: 800,
    height: 600
  });
  //win.setMenu(null);
  win.loadFile(__dirname + "/renderizador/index.html");
  win.on("closed", () => {
    win = null;
  });
}

app.on("ready", createWindow);

app.on("window-all-closed", () => {
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", () => {
  if (win === null) {
    createWindow();
  }
});

ipcMain.on("obtenerZonas", function(e, args) {
  axios.get(`${FLASK_SERVER}/v1/get/zonas`).then(respuesta => {
    e.sender.send("obtenerZonasRespuesta", respuesta.data);
  });
});

ipcMain.on("obtenerIntersecciones", function(e, zonaId, delegacionId) {
  axios
    .get(
      `${FLASK_SERVER}/v1/get/delegacion/${delegacionId}/interseccion?cantidad=5`
    )
    .then(respuesta => {
      const intersecciones = respuesta.data;
      e.sender.send(
        "obtenerInterseccionesRespuesta",
        intersecciones.length > 0 ? [intersecciones[0]] : []
      );
    });
});

ipcMain.on("obtenerSemaforos", function(
  e,
  zonaId,
  delegacionId,
  interseccionId
) {
  axios
    .get(`${FLASK_SERVER}/v1/get/interseccion/${interseccionId}/semaforos`)
    .then(respuesta => {
      e.sender.send("obtenerSemaforosRespuesta", respuesta.data);
    });
});

ipcMain.on("cambiarCiclo", function(e, interseccionId, modo, ciclo, semiciclo) {
  axios
    .post(`${FLASK_SERVER}/v1/post/interseccion/${interseccionId}`, {
      modo,
      ciclo,
      semiciclo
    })
    .then(respuesta => {
      e.sender.send("cambiarCicloRespuesta", respuesta.data);
    });
});

ipcMain.on("cambiarPrioridad", function(e, semaforoId, prioridad) {
  axios
    .post(`${FLASK_SERVER}/v1/post/semaforo/${semaforoId}`, {
      prioridad
    })
    .then(respuesta => {
      e.sender.send("cambiarPrioridadRespuesta", respuesta.data);
    });
});

ipcMain.on("cambiarLuz", function(e, semaforoId, luz) {
  axios
    .post(`${FLASK_SERVER}/v1/post/semaforo/${semaforoId}/?luz=${luz}`, {
      accion: "luz"
    })
    .then(respuesta => {
      e.sender.send("cambiarLuzRespuesta", respuesta.data);
    });
});

setInterval(function() {
  axios.get(`${FLASK_SERVER}/v1/subetucommit`);
}, 1000);
