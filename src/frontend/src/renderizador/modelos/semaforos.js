import { ipcRenderer } from "electron";

function obtenerSemaforos(zonaId, delegacionId, interseccionId) {
  return new Promise((resolve, reject) => {
    ipcRenderer.on("obtenerSemaforosRespuesta", function(e, args) {
      resolve(args);
    });
    ipcRenderer.send("obtenerSemaforos", zonaId, delegacionId, interseccionId);
  });
}

function cambiarCiclo(idInterseccion, modo, ciclo, semiciclo) {
  return new Promise((resolve, reject) => {
    ipcRenderer.on("cambiarCicloRespuesta", function(e, args) {
      resolve(args);
    });
    ipcRenderer.send("cambiarCiclo", idInterseccion, modo, ciclo, semiciclo);
  });
}

function cambiarPrioridad(idSemaforo, prioridad) {
  return new Promise((resolve, reject) => {
    ipcRenderer.on("cambiarPrioridadRespuesta", function(e, args) {
      resolve(args);
    });
    ipcRenderer.send("cambiarPrioridad", idSemaforo, prioridad);
  });
}

function cambiarLuz(idSemaforo, luz) {
  return new Promise((resolve, reject) => {
    ipcRenderer.on("cambiarLuzRespuesta", function(e, args) {
      resolve(args);
    });
    ipcRenderer.send("cambiarLuz", idSemaforo, luz);
  });
}

export { obtenerSemaforos, cambiarCiclo, cambiarPrioridad, cambiarLuz };
