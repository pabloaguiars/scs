import randomColor from "randomcolor";

const cache = [];
function colores(limite) {
  let index = 0;
  return {
    next: function() {
      index = index % limite;
      if (cache[index]) {
        return { value: cache[index++], done: false };
      } else {
        return {
          value: (cache[index++] = randomColor({
            luminosity: "dark"
          })),
          done: false
        };
      }
    },
    reiniciar: function() {
      for (let i = 0; i < limite; i++) {
        cache[i] = randomColor({
          luminosity: "dark"
        });
      }
    }
  };
}

export default colores;
