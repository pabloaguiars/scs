function filtrarIntersecciones(intersecciones, interseccionId) {
  const resultado = [];
  for (let interseccion of intersecciones) {
    if (interseccion.id.toString().startsWith(interseccionId)) {
      resultado.push(interseccion);
    }
  }
  return resultado;
}

export { filtrarIntersecciones };
