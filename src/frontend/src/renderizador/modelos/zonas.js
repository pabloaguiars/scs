import { ipcRenderer } from "electron";

function obtenerZonas() {
  return new Promise((resolve, reject) => {
    ipcRenderer.on("obtenerZonasRespuesta", function(e, args) {
      resolve(args);
    });
    ipcRenderer.send("obtenerZonas");
  });
}

function obtenerIntersecciones(zonaId, delegacionId) {
  return new Promise((resolve, reject) => {
    ipcRenderer.on("obtenerInterseccionesRespuesta", function(e, args) {
      resolve(args);
    });
    ipcRenderer.send("obtenerIntersecciones", zonaId, delegacionId);
  });
}

export { obtenerZonas, obtenerIntersecciones };
