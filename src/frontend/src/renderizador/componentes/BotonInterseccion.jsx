import React from "react";
import ReactDOM from "react-dom";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Icon from "@material-ui/core/Icon";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import CancelIcon from "@material-ui/icons/Cancel";
import { Link } from "react-router-dom";

function styles(theme) {
  return {
    flex: {
      display: "flex"
    },
    estado: {
      paddingRight: 5
    },
    button: {
      margin: theme.spacing.unit
    },
    fill: {
      width: "100%"
    }
  };
}

function BotonIntereccion(props) {
  const { classes, zonaId, delegacionId, interseccion } = props;
  return (
    <Card className={classes.fill}>
      <CardContent className={classes.fill}>
        <div className={classes.flex}>
          <Icon className={classes.estado}>
            {interseccion.estado == "activo" ? (
              <CheckCircleIcon />
            ) : (
              <CancelIcon />
            )}
          </Icon>
          <Typography color="textPrimary">{interseccion.id}</Typography>
        </div>
        <Typography color="textSecondary">Modo: {interseccion.modo}</Typography>
        <Typography color="textSecondary">
          Direccion: {interseccion.direccion}
        </Typography>
        <br />
        <Link
          to={`/zonas/${zonaId}/delegaciones/${delegacionId}/intersecciones/${
            interseccion.id
          }`}
          style={{ textDecoration: "none" }}
        >
          <Button color="primary" className={classes.button}>
            Detalles
          </Button>
        </Link>
      </CardContent>
    </Card>
  );
}

BotonIntereccion.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(BotonIntereccion);
