import React from "react";
import ReactDOM from "react-dom";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import Icon from "@material-ui/core/Icon";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import SendIcon from "@material-ui/icons/Send";
import TextField from "@material-ui/core/TextField";
import FormControl from "@material-ui/core/FormControl";
import TrafficIcon from "@material-ui/icons/Traffic";

function styles(theme) {
  return {
    fill: {
      width: "100%"
    },
    flex: {
      display: "flex"
    },
    derecha: {
      position: "absolute",
      right: 25,
      top: 25
    }
  };
}

function Semaforo(props) {
  const {
    classes,
    enCambiarLuz,
    enCambiarPrioridad,
    enCambio,
    mascara,
    semaforo
  } = props;
  return (
    <Card className={classes.fill}>
      <CardContent className={classes.fill}>
        <div className={classes.flex}>
          <Icon>
            <TrafficIcon />
          </Icon>
          <Typography variant="h6">{semaforo.id}</Typography>
          <div className={classes.derecha}>
            <FormControl>
              <TextField
                label="Prioridad"
                onChange={enCambio}
                value={mascara || semaforo.prioridad}
              />
            </FormControl>
            <IconButton
              color="primary"
              aria-label="Enviar"
              onClick={enCambiarPrioridad(mascara || semaforo.prioridad)}
            >
              <SendIcon />
            </IconButton>
          </div>
        </div>
        <div className={classes.flex}>
          <Button color="primary" onClick={enCambiarLuz("verde")}>
            Siga
          </Button>
          <Button color="secondary" onClick={enCambiarLuz("roja")}>
            Alto
          </Button>
          <Button color="secondary" onClick={enCambiarLuz("intermitente")}>
            Intermitente
          </Button>
        </div>
      </CardContent>
    </Card>
  );
}

Semaforo.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Semaforo);
