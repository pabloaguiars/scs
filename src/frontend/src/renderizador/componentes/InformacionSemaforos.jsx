import React from "react";
import ReactDOM from "react-dom";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import {
  LineChart,
  ResponsiveContainer,
  ReferenceLine,
  XAxis,
  YAxis,
  Label,
  Line,
  Legend,
  CartesianGrid
} from "recharts";
import colores from "../modelos/colores";

function styles(theme) {
  return {};
}

function lineas(semaforos) {
  const hexadecimales = colores(20);
  const lineas = [];
  for (let semaforo of semaforos) {
    lineas.push(
      <Line
        key={semaforo.id}
        type="monotone"
        dataKey={semaforo.id}
        stroke={hexadecimales.next().value}
      />
    );
  }
  lineas.push(
    <ReferenceLine key={"referencia"} y={100} ifOverflow="extendDomain" />
  );
  return lineas;
}

function datos(semaforos) {
  const datos = [];
  for (let semaforo of semaforos) {
    for (let i in semaforo.datos) {
      if (datos[i]) {
        datos[i][semaforo.id] = semaforo.datos[i] * 100;
      } else {
        datos[i] = { [semaforo.id]: semaforo.datos[i] * 100 };
      }
    }
  }
  return datos;
}

function InformacionSemaforos(props) {
  const { classes, semaforos } = props;
  return (
    <ResponsiveContainer width="100%" height={200}>
      <LineChart data={datos(semaforos)}>
        <XAxis>
          <Label value="Ciclos" offset={0} position="insideBottom" />
        </XAxis>
        <YAxis>
          <Label
            value="Capacidad"
            angle={-90}
            position="insideLeft"
            textAnchor="middle"
          />
        </YAxis>
        <CartesianGrid stroke="#eee" strokeDasharray="5 5" />
        {lineas(semaforos)}
        <Legend
          verticalAlign="bottom"
          wrapperStyle={{ position: "relative" }}
          height={36}
        />
      </LineChart>
    </ResponsiveContainer>
  );
}

InformacionSemaforos.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(InformacionSemaforos);
