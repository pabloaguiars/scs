import React from "react";
import ReactDOM from "react-dom";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Semaforo from "./Semaforo";

function styles(theme) {
  return {
    fill: {
      width: "100%"
    }
  };
}

function ListaSemaforos(props) {
  const {
    classes,
    enCambiarLuz,
    enCambiarPrioridad,
    enCambio,
    mascara,
    semaforos
  } = props;
  return (
    <List className={classes.fill}>
      {semaforos.map(semaforo => (
        <ListItem className={classes.fill} key={semaforo.id}>
          <Semaforo
            enCambiarPrioridad={enCambiarPrioridad(semaforo.id)}
            enCambiarLuz={enCambiarLuz(semaforo.id)}
            enCambio={enCambio(semaforo.id)}
            mascara={mascara[semaforo.id]}
            semaforo={semaforo}
          />
        </ListItem>
      ))}
    </List>
  );
}

ListaSemaforos.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(ListaSemaforos);
