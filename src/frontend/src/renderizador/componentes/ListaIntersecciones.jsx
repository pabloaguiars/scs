import React from "react";
import ReactDOM from "react-dom";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import BotonIntereccion from "./BotonInterseccion";

function styles(theme) {
  return {
    fill: {
      width: "100%"
    }
  };
}

function ListaIntersecciones(props) {
  const { classes, zonaId, delegacionId, intersecciones } = props;
  return (
    <List className={classes.fill}>
      {intersecciones.map(interseccion => (
        <ListItem className={classes.fill} key={interseccion.id}>
          <BotonIntereccion
            zonaId={zonaId}
            delegacionId={delegacionId}
            interseccion={interseccion}
          />
        </ListItem>
      ))}
    </List>
  );
}

ListaIntersecciones.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(ListaIntersecciones);
