import React from "react";
import ReactDOM from "react-dom";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import {
  LineChart,
  ReferenceLine,
  XAxis,
  YAxis,
  Label,
  Line,
  Legend,
  CartesianGrid
} from "recharts";
import colores from "../modelos/colores";

function styles(theme) {
  return {};
}

function lineas(intersecciones) {
  const hexadecimales = colores(20);
  const lineas = [];
  for (let interseccion of intersecciones) {
    lineas.push(
      <Line
        key={interseccion.id}
        type="monotone"
        dataKey={interseccion.id}
        stroke={hexadecimales.next().value}
      />
    );
  }
  lineas.push(
    <ReferenceLine key={"referencia"} y={100} ifOverflow="extendDomain" />
  );
  return lineas;
}

function datos(intersecciones) {
  const datos = [];
  for (let interseccion of intersecciones) {
    for (let i in interseccion.datos) {
      if (datos[i]) {
        datos[i][interseccion.id] = interseccion.datos[i] * 100;
      } else {
        datos[i] = { [interseccion.id]: interseccion.datos[i] * 100 };
      }
    }
  }
  return datos;
}

function InformacionIntersecciones(props) {
  const { classes, intersecciones } = props;
  return (
    <LineChart width={500} height={300} data={datos(intersecciones)}>
      <XAxis>
        <Label value="Ciclos" offset={0} position="insideBottom" />
      </XAxis>
      <YAxis>
        <Label
          value="Capacidad"
          angle={-90}
          position="insideLeft"
          textAnchor="middle"
        />
      </YAxis>
      <CartesianGrid stroke="#eee" strokeDasharray="5 5" />
      {lineas(intersecciones)}
      <Legend
        verticalAlign="bottom"
        wrapperStyle={{ position: "relative" }}
        height={36}
      />
    </LineChart>
  );
}

InformacionIntersecciones.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(InformacionIntersecciones);
