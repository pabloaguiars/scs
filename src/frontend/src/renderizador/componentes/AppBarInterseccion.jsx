import React, { Component } from "react";
import ReactDOM from "react-dom";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import FormControl from "@material-ui/core/FormControl";
import IconButton from "@material-ui/core/IconButton";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import { Link } from "react-router-dom";

function styles(theme) {
  return {};
}

class AppBarInterseccion extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { classes, zonaId, delegacionId, interseccionId } = this.props;
    return (
      <AppBar position="sticky">
        <Toolbar>
          <FormControl>
            <Link to={`/zonas/${zonaId}/delegaciones/${delegacionId}`}>
              <IconButton>
                <ArrowBackIcon />
              </IconButton>
            </Link>
          </FormControl>
          <FormControl>
            <Typography variant="h6" color="textPrimary">
              {interseccionId}
            </Typography>
          </FormControl>
        </Toolbar>
      </AppBar>
    );
  }
}

AppBarInterseccion.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(AppBarInterseccion);
