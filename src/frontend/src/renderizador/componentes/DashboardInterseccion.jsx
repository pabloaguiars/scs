import React, { Component } from "react";
import ReactDOM from "react-dom";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import FormatPaintIcon from "@material-ui/icons/FormatPaint";
import SendIcon from "@material-ui/icons/Send";
import Typography from "@material-ui/core/Typography";
import LinearProgress from "@material-ui/core/LinearProgress";
import AppBarInterseccion from "./AppBarInterseccion";
import InformacionSemaforos from "./InformacionSemaforos";
import ListaSemaforos from "./ListaSemaforos";
import { obtenerIntersecciones } from "../modelos/zonas";
import {
  obtenerSemaforos,
  cambiarCiclo,
  cambiarPrioridad,
  cambiarLuz
} from "../modelos/semaforos";
import colores from "../modelos/colores";

function styles(theme) {
  return {
    contenedor: {
      padding: 25,
      boxSizing: "border-box"
    },
    flex: {
      display: "flex",
      padding: 25
    },
    campo: {
      width: "100%",
      marginRight: 10
    }
  };
}

class DashboardInterseccion extends Component {
  constructor(props) {
    super(props);
    this.state = {
      interseccion: {},
      semaforos: [],
      mascaraInterseccion: {},
      mascaraSemaforos: {},
      horaCambio: 0,
      completado: 0
    };
  }

  enCambioModo = evento => {
    const mascaraInterseccion = Object.assign(
      {},
      this.state.mascaraInterseccion
    );
    mascaraInterseccion.modo = evento.target.value;
    this.setState({
      mascaraInterseccion: mascaraInterseccion
    });
  };

  enCambioCiclo = evento => {
    const mascaraInterseccion = Object.assign(
      {},
      this.state.mascaraInterseccion
    );
    mascaraInterseccion.ciclo = evento.target.value;
    this.setState({
      mascaraInterseccion: mascaraInterseccion
    });
  };

  enCambioSemiciclo = evento => {
    const mascaraInterseccion = Object.assign(
      {},
      this.state.mascaraInterseccion
    );
    mascaraInterseccion.semiciclo = evento.target.value;
    this.setState({
      mascaraInterseccion: mascaraInterseccion
    });
  };

  enCambioPrioridad = id => evento => {
    const mascaraSemaforos = Object.assign({}, this.state.mascaraSemaforos);
    mascaraSemaforos[id] = evento.target.value;
    this.setState({
      mascaraSemaforos: mascaraSemaforos
    });
  };

  actualizarInformacion() {
    const { zonaId, delegacionId, interseccionId } = this.props.match.params;
    obtenerIntersecciones(zonaId, delegacionId).then(
      function(intersecciones) {
        const interseccion = intersecciones
          .filter(function(interseccion) {
            return interseccion.id == interseccionId;
          })
          .concat([{}])[0];
        obtenerSemaforos(zonaId, delegacionId, interseccionId).then(
          function(semaforos) {
            const fecha = new Date();
            const hora = fecha.getTime();
            this.setState({
              interseccion: interseccion,
              semaforos: semaforos,
              horaCambio: hora + interseccion.ciclo * 1000, // TODO arreglar esto
              completado: 0
            });
          }.bind(this)
        );
      }.bind(this)
    );
    this.timer = setInterval(this.progresar, 500);
  }

  componentDidMount() {
    this.actualizarInformacion();
  }

  componentWillUnmount() {
    clearInterval(this.timer);
  }

  progresar = () => {
    const fecha = new Date();
    const hora = fecha.getTime();
    const completado =
      100 -
      ((this.state.horaCambio - hora) /
        (this.state.interseccion.ciclo * 1000)) *
        100;
    if (completado >= 100) {
      this.actualizarInformacion();
      clearInterval(this.timer);
    } else {
      this.setState({
        completado: completado
      });
    }
  };

  cambioColores = e => {
    const paleta = colores(20);
    paleta.reiniciar();
    this.actualizarInformacion();
  };

  enCambiarCiclo = evento => {
    const { mascaraInterseccion, interseccion } = this.state;
    cambiarCiclo(
      interseccion.id,
      mascaraInterseccion.modo || interseccion.modo || "",
      mascaraInterseccion.ciclo || interseccion.ciclo || "",
      mascaraInterseccion.semiciclo || interseccion.semiciclo || ""
    );
  };

  enCambiarPrioridad = idSemaforo => prioridad => evento => {
    cambiarPrioridad(idSemaforo, prioridad);
  };

  enCambiarLuz = idSemaforo => luz => evento => {
    cambiarLuz(idSemaforo, luz);
  };

  render() {
    const { classes } = this.props;
    const { zonaId, delegacionId, interseccionId } = this.props.match.params;
    const {
      mascaraInterseccion,
      interseccion,
      mascaraSemaforos,
      semaforos
    } = this.state;
    const {
      enCambiarCiclo,
      enCambiarPrioridad,
      enCambiarLuz,
      enCambioCiclo,
      enCambioSemiciclo,
      enCambioModo,
      enCambioPrioridad
    } = this;
    return (
      <div>
        <AppBarInterseccion
          zonaId={zonaId}
          delegacionId={delegacionId}
          interseccionId={interseccionId}
        />
        <div style={{ position: "fixed", width: "100%", zIndex: 999 }}>
          <LinearProgress variant="determinate" value={this.state.completado} />
        </div>
        <div>
          <div className={classes.contenedor}>
            <InformacionSemaforos semaforos={semaforos || []} />
          </div>
          <div className={classes.flex}>
            <FormControl className={classes.campo}>
              <InputLabel htmlFor="">Modo</InputLabel>
              <Select
                value={mascaraInterseccion.modo || interseccion.modo || ""}
                onChange={enCambioModo}
              >
                <MenuItem value="">Ninguno</MenuItem>
                <MenuItem value="auto">Automático</MenuItem>
                <MenuItem value="ciclo">Ciclo</MenuItem>
              </Select>
            </FormControl>
            <FormControl className={classes.campo}>
              <TextField
                label="Ciclo"
                value={mascaraInterseccion.ciclo || interseccion.ciclo || ""}
                onChange={enCambioCiclo}
              />
            </FormControl>
            <FormControl className={classes.campo}>
              <TextField
                label="Semiciclo"
                value={
                  mascaraInterseccion.semiciclo || interseccion.semiciclo || ""
                }
                onChange={enCambioSemiciclo}
              />
            </FormControl>
            <IconButton
              color="primary"
              aria-label="Enviar"
              onClick={enCambiarCiclo}
            >
              <SendIcon />
            </IconButton>
          </div>
          <div>
            <ListaSemaforos
              enCambiarPrioridad={enCambiarPrioridad}
              enCambiarLuz={enCambiarLuz}
              enCambio={enCambioPrioridad}
              mascara={mascaraSemaforos}
              semaforos={semaforos}
            />
          </div>
          <Button
            variant="fab"
            color="primary"
            aria-label="Repintar"
            style={{ position: "fixed", bottom: 10, right: 10 }}
            onClick={this.cambioColores}
          >
            <FormatPaintIcon />
          </Button>
        </div>
      </div>
    );
  }
}

DashboardInterseccion.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(DashboardInterseccion);
