import React, { Component } from "react";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Icon from "@material-ui/core/Icon";
import FormatPaintIcon from "@material-ui/icons/FormatPaint";
import AppBarZonas from "./AppBarZonas";
import { obtenerZonas, obtenerIntersecciones } from "../modelos/zonas";
import { filtrarIntersecciones } from "../modelos/intersecciones";
import colores from "../modelos/colores";
import ListaIntersecciones from "./ListaIntersecciones";
import InformacionIntersecciones from "./InformacionIntersecciones";
import "core-js/fn/array/flat-map";

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      zonas: [],
      zonaId: "",
      delegaciones: [],
      delegacionId: "",
      intersecciones: [],
      busqueda: "",
      mostradas: [],
      timeout: null
    };
  }

  componentDidMount() {
    const params = this.props.match.params;
    if (params.zonaId && params.delegacionId) {
      const { zonaId, delegacionId } = params;
      obtenerZonas().then(
        function(zonas) {
          const delegaciones = zonas
            .filter(function(zona) {
              return zona.id == zonaId;
            })
            .flatMap(function(zona) {
              return zona.delegaciones;
            });
          obtenerIntersecciones(zonaId, delegacionId).then(
            function(intersecciones) {
              this.setState({
                zonas: zonas,
                zonaId: zonaId,
                delegacionId: delegacionId,
                delegaciones: delegaciones,
                intersecciones: intersecciones,
                mostradas: intersecciones,
                timeout: setTimeout(this.actualizarInformacion, 60 * 1000)
              });
            }.bind(this)
          );
        }.bind(this)
      );
    } else {
      obtenerZonas().then(
        function(zonas) {
          this.setState({
            zonas: zonas
          });
        }.bind(this)
      );
    }
  }

  componentWillUnmount() {
    clearTimeout(this.state.timeout);
  }

  enCambioZonaId = evento => {
    const { zonas, busqueda, timeout } = this.state;
    const zonaId = evento.target.value;
    const delegaciones = zonas
      .filter(function(zona) {
        return zona.id == zonaId;
      })
      .flatMap(function(zona) {
        return zona.delegaciones;
      });
    const delegacionId = delegaciones.length > 0 ? delegaciones[0].id : "";
    obtenerIntersecciones(zonaId, delegacionId).then(
      function(intersecciones) {
        clearTimeout(timeout);
        this.setState({
          zonaId: zonaId,
          delegacionId: delegacionId,
          delegaciones: delegaciones,
          intersecciones: intersecciones,
          mostradas: filtrarIntersecciones(intersecciones, busqueda),
          timeout: setTimeout(this.actualizarInformacion, 60 * 1000)
        });
      }.bind(this)
    );
  };

  enCambioDelegacionId = evento => {
    const { zonaId, busqueda, timeout } = this.state;
    const delegacionId = evento.target.value;
    obtenerIntersecciones(zonaId, delegacionId).then(
      function(intersecciones) {
        clearTimeout(timeout);
        this.setState({
          delegacionId: delegacionId,
          intersecciones: intersecciones,
          mostradas: filtrarIntersecciones(intersecciones, busqueda),
          timeout: setTimeout(this.actualizarInformacion, 60 * 1000)
        });
      }.bind(this)
    );
  };

  enCambioBusqueda = evento => {
    const { intersecciones, timeout } = this.state;
    const busqueda = evento.target.value;
    const mostradas = filtrarIntersecciones(intersecciones, busqueda);
    clearTimeout(timeout);
    this.setState({
      busqueda: busqueda,
      mostradas: mostradas,
      timeout:
        mostradas.length > 0
          ? setTimeout(this.actualizarInformacion, 60 * 1000)
          : null
    });
    this.setState({
      busqueda: busqueda
    });
  };

  actualizarInformacion = () => {
    const { zonaId, delegacionId, busqueda, timeout } = this.state;
    obtenerIntersecciones(zonaId, delegacionId).then(
      function(intersecciones) {
        clearTimeout(timeout);
        this.setState({
          intersecciones: intersecciones,
          mostradas: filtrarIntersecciones(intersecciones, busqueda),
          timeout: setTimeout(this.actualizarInformacion, 60 * 1000) // TODO fix memory leak?
        });
      }.bind(this)
    );
  };

  cambioColores = e => {
    const paleta = colores(20);
    paleta.reiniciar();
    this.actualizarInformacion();
  };

  render() {
    const { zonas, zonaId, delegaciones, delegacionId, mostradas } = this.state;
    const mensaje = (
      <div style={{ padding: 25 }}>
        <Typography variant="h6">No hay intersecciones disponibles.</Typography>
      </div>
    );
    const informacion = (
      <div style={{ display: "flex" }}>
        <div style={{ width: 600, padding: 25, boxSizing: "border-box" }}>
          <InformacionIntersecciones intersecciones={mostradas} />
        </div>
        <div style={{ width: "100%" }}>
          <ListaIntersecciones
            zonaId={zonaId}
            delegacionId={delegacionId}
            intersecciones={mostradas}
          />
        </div>
      </div>
    );
    return (
      <div>
        <AppBarZonas
          zonas={zonas}
          zonaId={zonaId}
          delegaciones={delegaciones}
          delegacionId={delegacionId}
          enCambioZonaId={this.enCambioZonaId}
          enCambioDelegacionId={this.enCambioDelegacionId}
          enCambioBusqueda={this.enCambioBusqueda}
        />
        {mostradas.length > 0 ? informacion : mensaje}
        <Button
          variant="fab"
          color="primary"
          aria-label="Repintar"
          style={{ position: "fixed", bottom: 10, right: 10 }}
          onClick={this.cambioColores}
        >
          <FormatPaintIcon />
        </Button>
      </div>
    );
  }
}

export default Dashboard;
