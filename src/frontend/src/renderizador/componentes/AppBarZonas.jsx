import React, { Component } from "react";
import ReactDOM from "react-dom";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import TextField from "@material-ui/core/TextField";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";

function styles(theme) {
  return {
    fill: {
      margin: 10,
      width: "100%"
    },
    formControl: {
      width: 200,
      margin: 10
    }
  };
}

function AppBarZonas(props) {
  const {
    classes,
    zonas,
    zonaId,
    delegaciones,
    delegacionId,
    intersecciones,
    busqueda,
    enCambioZonaId,
    enCambioDelegacionId,
    enCambioBusqueda
  } = props;
  return (
    <AppBar position="sticky">
      <Toolbar>
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="">Zonas</InputLabel>
          <Select value={zonaId} onChange={enCambioZonaId}>
            <MenuItem key="" value="">
              Ninguna
            </MenuItem>
            {zonas.map(zona => (
              <MenuItem key={zona.id} value={zona.id}>
                {zona.nombre}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="">Delegacion</InputLabel>
          <Select value={delegacionId} onChange={enCambioDelegacionId}>
            <MenuItem key="" value="">
              Ninguna
            </MenuItem>
            {delegaciones.map(delegacion => (
              <MenuItem key={delegacion.id} value={delegacion.id}>
                {delegacion.nombre}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        <FormControl className={classes.fill}>
          <TextField
            type="search"
            value={busqueda}
            onChange={enCambioBusqueda}
            label="Buscar interseccion"
          />
        </FormControl>
      </Toolbar>
    </AppBar>
  );
}

AppBarZonas.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(AppBarZonas);
