import React, { Component } from "react";
import Dashboard from "./Dashboard";
import DashboardInterseccion from "./DashboardInterseccion";
import { HashRouter as Router, Route } from "react-router-dom";

class App extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Router>
        <div>
          <Route exact path="/" component={Dashboard} />
          <Route
            exact
            path="/zonas/:zonaId/delegaciones/:delegacionId"
            component={Dashboard}
          />
          <Route
            exact
            path="/zonas/:zonaId/delegaciones/:delegacionId/intersecciones/:interseccionId"
            component={DashboardInterseccion}
          />
        </div>
      </Router>
    );
  }
}

export default App;
