# SCS front-end
SCS front-end es un GUI para controlar semáforos a través del [sistema de back-end de scs](../backend). Está desarrollado en electron por sus capacidades multiplataforma y utiliza el framework react para renderizar contenido.

## Requisitos
El entorno de trabajo que utiliza SCS front-end requiere tener instalado [nodejs y npm](https://nodejs.org/) por lo que se sugiere leer la documentación respectiva de cada uno para su instalación.

* Probado con nodejs v8.12.0
* Probado con npm 6.4.1

## ¿Cómo iniciar?
El primer paso es instalar las dependencias de la aplicación:
```shell
npm install
```
El siguiente paso es transpilar la aplicación usando babel para poder usar jsx en react:
```shell
npm run build
```

Por último se puede comenzar la aplicación con el comando:
```shell
npm run start
```
