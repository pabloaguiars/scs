# Misc
This folder contains the source for the backend part of the project.
Its written in python.

# Where to Start

# API
## GET /v1/zonas/
Output:
```json
[
  {
    "id": "foo",
    "nombre": "zona 1",
    "delegaciones": [
      {"id": "foo","nombre": "delegacion 1"},
      {"id": "bar","nombre": "delegacion 2"}
    ]
  },
  {
    "id": "bar",
    "nombre": "zona 2",
    "delegaciones": [
      {"id": "foo","nombre": "delegacion 1"},
      {"id": "bar","nombre": "delegacion 2"}
    ]
  },
]
```
## GET /v1/zonas/:id/delegaciones/:id/intersecciones/?cantidad=n
Output:
```json
[
  {
    "id": "foo",
    "direccion": "direccion 1",
    "estado": "activo",
    "tipo": "cruce",
    "modo": "auto",
    "ciclo": 300,
    "semiciclo": 200,
    "comenzado": "Sun Nov 25 12:48:14 PST 2018",
    "datos": [0.0, 0.2, 0.4, 0.5, 0.5]
  },
  {
    "id": "bar",
    "direccion": "direccion2",
    "estado": "inactivo",
    "tipo": "vuelta",
    "modo": "ciclo",
    "ciclo": 400,
    "semiciclo": 100,
    "comenzado": "Sun Nov 25 12:48:14 PST 2018",
    "datos": [0.0, 0.1, 0.2, 0.2, 0.2]
  }
]
```
## POST /v1/zonas/:id/delegaciones/:id/intersecciones/:id
Input:
```json
{
  "modo": "auto",
  "ciclo": 300,
  "semiciclo": 200
}
```

## GET /v1/zonas/:id/delegaciones/:id/intersecciones/:id/semaforos/?cantidad=n
Output:
```json
[
  {
    "id": "foo",
    "prioridad": 1,
    "datos": [0.0, 0.2, 0.4, 0.5, 0.6]
  },
  {
    "id": "bar",
    "prioridad": 4,
    "datos": [0.5, 0.1, 0.6, 0.1, 0.8]
  }
]
```

## POST /v1/zonas/:id/delegaciones/:id/intersecciones/:id/semaforos/:id
Input:
```json
{
  "prioridad": 3
}
```
Output:
```
HT200 OK
```

## POST /v1/zonas/:id/delegaciones/:id/intersecciones/:id/semaforos/:id/siga
Output:
```
HT200 OK
```

## POST /v1/zonas/:id/delegaciones/:id/intersecciones/:id/semaforos/:id/alto
Output:
```
HT200 OK

```
## POST /v1/zonas/:id/delegaciones/:id/intersecciones/:id/semaforos/:id/intermitente
Output:
```
HT200 OK
```
