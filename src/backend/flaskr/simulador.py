def promedio(numeros):
    return float(sum(numeros)) / max(len(numeros), 1)

class Semaforo:
    def __init__(self, verde, amarillo, rojo, llegadas):
        self.verde = verde
        self.amarillo = amarillo
        self.rojo = rojo
        self.llegadas = llegadas;
        self.salidas = salidas;

class Interseccion:
    def __init__(self, semaforos):
        self.semaforos = semaforos

class Simulacion:
    def __init__(self, datos):
        self.datos = datos

    def simular(parametros):
        """ Cuánto tiempo tarda el semáforo con esos parámetros """
        for semaforos in datos["semaforos"]:
            transito = promedio(semaforos["datos"])
