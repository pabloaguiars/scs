# Se encarga de los request post, en general recibe datos del url
# y de json para modificar la base de datos y dar instruciones al
# arduino
from flaskr import arduino_serial
import json
from flask import Blueprint, jsonify, abort, make_response, request
from flaskr.db import db_connect
from psycopg2 import sql
# from io_arduino import arduino_post

bp = Blueprint('post', __name__,url_prefix='/v1/post')

# Se cambian los valores de ciclo de la intersección deseada
@bp.route('/interseccion/<int:id_interseccion>', methods=['POST'])
def mod_ciclo(id_interseccion):
    json = []
    json = request.get_json()
    if len(json) == 0:
        abort(400)
    arduino_serial.write(json,id_interseccion)
    return make_response('OK',200)

# Se cambia la prioridad del semaforo escogido
@bp.route('/semaforo/<int:id_semaforo>', methods=['POST'])
def mod_prioridad(id_semaforo):
    json = []
    json = request.get_json()
    if len(json) == 0:
        abort(400)
    db = db_connect()
    cur = db.cursor()
    cur.execute(
        sql.SQL("""
            UPDATE semaforo
            SET prioridad = %s
            WHERE idsemaforo = %s"""
        ),[json["prioridad"],id_semaforo]
    )
    db.commit()
    db.close()
    return make_response('OK',200)

# Se cambia la luz que actualmente este en el semaforo a la deseada
@bp.route('/semaforo/<int:id_semaforo>/', methods=['POST'])
def mod_luz(id_semaforo):
    json = []
    json = request.get_json()
    luz = request.args.get('luz', type=str  )
    if len(json) == 0:
        abort(400)
    arduino_serial.write(json)
    return make_response('OK',200)

@bp.errorhandler(400)
def bad_request(error):
    return make_response(jsonify({'error' : 'Bad request'}), 400)