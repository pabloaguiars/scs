# Se encarga de los request get, en general se comunica con la base de 
# datos y regresa lo requerido en un json

import json
from flask import Blueprint, jsonify, abort, make_response, request
from flaskr.db import db_connect
from psycopg2 import sql

bp = Blueprint('get', __name__,url_prefix='/v1/get')

# Regresa las zonas y delegaciones de cada zona
@bp.route('/zonas', methods=['GET'])
def get_zonas():
    db = db_connect()
    cur = db.cursor()
    cur.execute(
        sql.SQL("""SELECT * FROM Zona""")
    )
    zonas = cur.fetchall()
    cur.execute("""SELECT * FROM Delegacion""")
    delegaciones = cur.fetchall()
    json = []
    for count, zona in enumerate(zonas):
        json.append({"id": zona[0],
        "nombre": zona[1],
        "delegaciones" : [] 
        })
        for delegacion in delegaciones:
             if delegacion[1] == zona[0]:
                 json[count]['delegaciones'].append({"id": delegacion[0],"nombre": delegacion[2]})
    if len(json) == 0:
        abort(404)
    return jsonify(json)

# Regresa las intersecciones de una delegacion
@bp.route('/delegacion/<int:id_delegacion>/interseccion', methods=['GET'])
def get_intersecciones(id_delegacion):
    db = db_connect()
    cur = db.cursor()
    limit = request.args.get('cantidad', default=5, type=int)
    json = []
    cur.execute(
        sql.SQL("""SELECT interseccion.idinterseccion, direccioninterseccion, estado, descripciontipointerseccion, descripcionmodo,ciclo, semiciclo,fechahora
            FROM interseccion 
            JOIN tipointerseccion 
            ON interseccion.idtipointerseccion = tipointerseccion.idtipointerseccion
            JOIN ciclosensado 
            ON interseccion.idinterseccion = ciclosensado.idinterseccion
            JOIN modo
            ON ciclosensado.idmodo = modo.idmodo
            WHERE iddelegacion = %s"""
        ),[id_delegacion])
    intersecciones = cur.fetchall()
    for count, interseccion in enumerate(intersecciones):
        json.append({"id":interseccion[0],
        "direccion" : interseccion[1],
        "estado" : interseccion[2],
        "tipo" : interseccion[3],
         "modo" : interseccion[4],
        "ciclo": interseccion[5],
        "semiciclo": interseccion[6],
        "comenzado": interseccion[7]
        })
        cur.execute(
            sql.SQL("""SELECT AVG(sensortiempoactivoenverde+sensortiemponoactivoenrojo)/2.0 AS tiempoOcupado, fechahora
                FROM datossensados JOIN ciclosensado
                ON datossensados.idciclosensado  = ciclosensado.idciclosensado
                WHERE datossensados.idciclosensado in (
                    SELECT idciclosensado
                    FROM ciclosensado
                    WHERE idinterseccion = %s
                    ORDER by fechahora DESC
                    LIMIT %s
                    )
                GROUP BY datossensados.idciclosensado, fechahora
                ORDER BY fechahora DESC
                """
            ),[interseccion[0],limit])
        datos = cur.fetchall()
        json[count]['datos'] = [dato[0] for dato in datos]
    if len(json) == 0:
        abort(404)
    return jsonify(json)

# Regresa los semaforos de una interseccion asi como un promedio de los ultimos datos
@bp.route('/interseccion/<int:id_interseccion>/semaforos', methods=['GET'])
def get_semaforos(id_interseccion):
    db = db_connect()
    cur = db.cursor()
    limit = request.args.get('cantidad', default=5, type=int)
    cur.execute(
        sql.SQL("""SELECT idsemaforo, prioridad
            FROM semaforo
            WHERE idinterseccion = %s"""
        ),[id_interseccion])
    semaforos = cur.fetchall()
    json = []
    for count, semaforo in enumerate(semaforos):
        json.append({"id": semaforo[0],
        "prioridad": semaforo[1],
        })
        cur.execute(
            sql.SQL("""SELECT (sensortiempoactivoenverde+sensortiemponoactivoenrojo)/2.0 AS tiempoOcupado, fechahora
                FROM datossensados JOIN ciclosensado
                ON datossensados.idciclosensado  = ciclosensado.idciclosensado
                WHERE datossensados.idciclosensado in (
                        SELECT idciclosensado
                        FROM ciclosensado
                        WHERE idinterseccion = %s
                        ORDER by fechahora DESC
                        LIMIT %s
                ) AND idsemaforo = %s
                ORDER BY fechahora DESC"""
                ),[id_interseccion,limit,semaforo[0]])
        datos = cur.fetchall()
        json[count]['datos'] = [dato[0] for dato in datos]
    if len(json) == 0:
         abort(404)
    return jsonify(json)

# En caso que no se haya podido crear el archivo json
@bp.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error' : 'Not found'}), 404)
