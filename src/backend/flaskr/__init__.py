from flask import Flask, jsonify, abort, make_response

from flaskr.db import db_connect

def create_app():
    app = Flask(__name__, instance_relative_config=True)

    from . import db
    db.init_app(app)

    from . import get
    app.register_blueprint(get.bp)

    from . import post
    app.register_blueprint(post.bp)
    
    from . import sync
    app.register_blueprint(sync.bp)


    return app

# @auth.get_password
# def get_password(username):
#     if username == 'Juan':
#         return 'Clave'

# @auth.error_handle
# def unathorized():
#     return make_response(jsonify({'error' : 'Unauthorized access'}), 401)