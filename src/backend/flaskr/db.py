# Se encarga de la conexión a la base de datos
import psycopg2
import click

# from flaskr.db_config import config
from flask import current_app, g
from flask.cli import with_appcontext

# Se conecta a la base de datos
def db_connect():
    if 'db' not in g:
            print('Connecting to SCS database...')
            g.db= psycopg2.connect(host="localhost",database="SCS", user="postgres", password="postgres")
            cur = g.db.cursor()
            print('PostgresSQL version')
            cur.execute('SELECT version()')
            db_version = cur.fetchone()
            print(db_version)
    return g.db

# Se cierra la conexión a la base de datos
def db_close(e=None):
    db = g.pop('db',None)
    if db is not None:
        db.close()
        print('Database connection closed.')

# Al iniciar la aplicación checa que este la base de datos presente
@click.command('db-init')
@with_appcontext
def db_connect_command():
    db_connect()

def init_app(app):
    app.teardown_appcontext(db_close)
    app.cli.add_command(db_connect_command)

