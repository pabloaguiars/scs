from flask import current_app, g
import serial, time, json

def write(data,id):
    if(id == 10):
        port = 'COM6'
        baud = 38400
    elif(id == 9):
        port = 'COM3'
        baud = 9600
    ArduinoSerial = serial.Serial(port,baud)
    time.sleep(2)
    data = json.dumps(data)
    byte = (data.encode('utf-8'))
    ArduinoSerial.write(byte)

