# Prequisites
Python3


# How to run

1. Start the virtual enviroment

Linux
```
.venv/bin/activate
```
Windows
```
venv\Scripts\activate
```
2. Set the FLASK_APP enviroment variable

Linux
```
export FLASK_APP=__init__.py
```
Windows (CMD)
```
set FLASK_APP=__init__.py
```

3. Start Flask

```
cd flaskr
flask run
```

## Note

This is the built in flask server, is not suitable for production. When deploying you should use a suitable deployment option. For different options see: http://flask.pocoo.org/docs/1.0/deploying/#deployment
